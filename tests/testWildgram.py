import unittest
from wildgram import wildgram, WildRules, WildForm, MagicRules,convertCodeDictToString


class TestWildgram(unittest.TestCase):

    def assertRangesEqual(self, ranges, index, token, start, end, type, parent=None):
        self.assertEqual(ranges[index]["token"], token)
        self.assertEqual(ranges[index]["startIndex"], start)
        self.assertEqual(ranges[index]["endIndex"], end)
        self.assertEqual(ranges[index]["tokenType"], type)
        self.assertEqual(ranges[index]["index"], index)
        if isinstance(parent, int):
            self.assertEqual(ranges[index]["parent"], parent)

    def test_basic_error(self):
        with self.assertRaises(Exception):
            wildgram(1)
        with self.assertRaises(Exception):
            wildgram()

    def test_basic_input(self):
        ranges = wildgram("boo")
        self.assertRangesEqual(ranges, 0, "boo", 0, 3, "token")
        ranges = wildgram("bool bahl", include1gram=False)
        self.assertRangesEqual(ranges, 0, "bool bahl", 0, 9, "token")

    def test_stop_words(self):
        ranges = wildgram("bats and fleas")
        self.assertRangesEqual(ranges, 0, "bats", 0, 4, "token")
        self.assertRangesEqual(ranges, 1, " and ", 4, 9, "noise")
        self.assertRangesEqual(ranges, 2, "fleas", 9, 14, "token")
        ranges = wildgram("and was beautiful")
        self.assertRangesEqual(ranges, 0, "and was ", 0, 8, "noise")
        self.assertRangesEqual(ranges, 1, "beautiful", 8, 17, "token")

    def test_empty(self):
        emptyCases = ["and", " and", "and ", "and was","  ", " ;"]
        for empty in emptyCases:
            ranges = wildgram(empty, returnNoise=False, includeParent=False)
            self.assertListEqual(ranges, [])
            ranges = wildgram(empty)
            self.assertRangesEqual(ranges, 0, empty, 0, len(empty), "noise")

    def test_mixed(self):
        ranges = wildgram("bats and fleas-ticks are vermin; they need to be erradicated!", include1gram=False, returnNoise=False, includeParent=False)
        self.assertRangesEqual(ranges, 0, "bats", 0, 4, "token")
        self.assertRangesEqual(ranges, 1, "fleas-ticks", 9, 20, "token")
        self.assertRangesEqual(ranges, 2, "vermin", 25, 31, "token")
        self.assertRangesEqual(ranges, 3, "need", 38, 42, "token")
        self.assertRangesEqual(ranges, 4, "need to be erradicated", 38, 60, "token")
        self.assertRangesEqual(ranges, 5, "erradicated", 49, 60, "token")

    def test_topic(self):
        ranges = wildgram("denies")
        self.assertRangesEqual(ranges, 0, "denies", 0, 6, "negation")
        ranges = wildgram("denies diabetes", returnNoise=False, includeParent=False)
        self.assertRangesEqual(ranges, 0, "denies", 0, 6, "negation")
        self.assertRangesEqual(ranges, 1, "diabetes", 7, 15, "token")
        ranges = wildgram("patient denies", returnNoise=False,includeParent=False)
        self.assertRangesEqual(ranges, 0, "patient", 0, 7, "token")
        self.assertRangesEqual(ranges, 1, "denies", 8, 14, "negation")
        ranges = wildgram("patient denies diabetes", returnNoise=False,includeParent=False)
        self.assertRangesEqual(ranges, 0, "patient", 0, 7, "token")
        self.assertRangesEqual(ranges, 1, "denies", 8, 14, "negation")
        self.assertRangesEqual(ranges, 2, "diabetes", 15, 23, "token")
        ranges = wildgram("5 mg/0.25ml")
        self.assertRangesEqual(ranges, 0, "5", 0, 1, "numeric")
        self.assertRangesEqual(ranges, 1, " ", 1, 2, "noise")
        self.assertRangesEqual(ranges, 2, "mg", 2, 4, "token")
        self.assertRangesEqual(ranges, 3, "/", 4, 5, "noise")
        self.assertRangesEqual(ranges, 4, "0.25", 5, 9, "numeric")
        self.assertRangesEqual(ranges, 5, "ml", 9, 11, "token")

    def test_numbers(self):
        ranges = wildgram("123 testing", returnNoise=False,includeParent=False)
        self.assertRangesEqual(ranges, 0, "123", 0, 3, "numeric")
        self.assertRangesEqual(ranges, 1, "testing", 4, 11, "token")
        ranges = wildgram("123 testing 1234", returnNoise=False,includeParent=False)
        self.assertRangesEqual(ranges, 0, "123", 0, 3, "numeric")
        self.assertRangesEqual(ranges, 1, "testing", 4, 11, "token")
        self.assertRangesEqual(ranges, 2, "1234", 12, 16, "numeric")
        ranges = wildgram("123mg/ml testing 1234mg 0.5-500 mg 0.5-500mg", returnNoise=False,includeParent=False)
        self.assertRangesEqual(ranges, 0, "123", 0, 3, "numeric")
        self.assertRangesEqual(ranges, 1, "mg/ml", 3, 8, "token")
        self.assertRangesEqual(ranges, 2, "testing", 9, 16, "token")
        self.assertRangesEqual(ranges, 3, "1234", 17, 21, "numeric")
        self.assertRangesEqual(ranges, 4, "mg", 21, 23, "token")
        self.assertRangesEqual(ranges, 5, "0.5", 24, 27, "numeric")
        self.assertRangesEqual(ranges, 6, "500", 28, 31, "numeric")
        self.assertRangesEqual(ranges, 7, "mg", 32, 34, "token")
        self.assertRangesEqual(ranges, 8, "0.5", 35, 38, "numeric")
        self.assertRangesEqual(ranges, 9, "500", 39, 42, "numeric")
        self.assertRangesEqual(ranges, 10, "mg", 42, 44, "token")

    def test_default_lists(self):
        ranges = wildgram("123 testing", [], [], include1gram=False)
        self.assertRangesEqual(ranges, 0, "123 testing", 0, 11, "token")
        ranges = wildgram("123 testing", [],include1gram=False, returnNoise=False, includeParent=False)
        self.assertRangesEqual(ranges, 0, "123", 0, 3, "numeric")
        self.assertRangesEqual(ranges, 1, "testing", 4, 11, "token")
        ranges = wildgram("123 testing is bad", topicwords=[],include1gram=False,returnNoise=False,includeParent=False)
        self.assertRangesEqual(ranges, 0, "123 testing", 0, 11, "token")
        self.assertRangesEqual(ranges, 1, "bad", 15, 18, "token")

    def test_new_lists(self):
        ranges = wildgram("123 testing is bad and the world is good", ["is"], [],include1gram=False, returnNoise=False,includeParent=False)
        self.assertRangesEqual(ranges, 0, "123 testing", 0, 11, "token")
        self.assertRangesEqual(ranges, 1, "bad and the world", 15, 32, "token")
        self.assertRangesEqual(ranges, 2, "good", 36, 40, "token")

        ranges = wildgram("123 testing is bad and the world is good", [], ["is"],include1gram=False, returnNoise=False,includeParent=False)
        self.assertRangesEqual(ranges, 0, "123 testing", 0, 11, "token")
        self.assertRangesEqual(ranges, 1, "is", 12, 14, "token")
        self.assertRangesEqual(ranges, 2, "bad and the world", 15, 32, "token")
        self.assertRangesEqual(ranges, 3, "is", 33, 35, "token")
        self.assertRangesEqual(ranges, 4, "good", 36, 40, "token")

    def test_1gram(self):
        ranges = wildgram("123 testing is bad and the world is good", include1gram=True, returnNoise=False,includeParent=False)
        self.assertRangesEqual(ranges, 0, "123", 0, 3, "numeric")
        self.assertRangesEqual(ranges, 1, "testing", 4, 11, "token")
        self.assertRangesEqual(ranges, 2, "bad", 15, 18, "token")
        self.assertRangesEqual(ranges, 3, "world", 27, 32, "token")
        self.assertRangesEqual(ranges, 4, "good", 36, 40, "token")

        ranges = wildgram("123 testing is bad and the world is -- goodly gracious sakes", include1gram=True, returnNoise=False,includeParent=False)
        self.assertRangesEqual(ranges, 0, "123", 0, 3, "numeric")
        self.assertRangesEqual(ranges, 1, "testing", 4, 11, "token")
        self.assertRangesEqual(ranges, 2, "bad", 15, 18, "token")
        self.assertRangesEqual(ranges, 3, "world", 27, 32, "token")
        self.assertRangesEqual(ranges, 4, "goodly", 39, 45, "token")
        self.assertRangesEqual(ranges, 5, "goodly gracious sakes", 39, 60, "token")
        self.assertRangesEqual(ranges, 6, "gracious", 46, 54, "token")
        self.assertRangesEqual(ranges, 7, "sakes", 55, 60, "token")

        ranges = wildgram("123 testing is bad and the world wildlife foundation is -- goodly gracious sakes", include1gram=True, returnNoise=False,includeParent=False)
        self.assertRangesEqual(ranges, 0, "123", 0, 3, "numeric")
        self.assertRangesEqual(ranges, 1, "testing", 4, 11, "token")
        self.assertRangesEqual(ranges, 2, "bad", 15, 18, "token")
        self.assertRangesEqual(ranges, 3, "world", 27, 32, "token")
        self.assertRangesEqual(ranges, 4, "world wildlife foundation", 27, 52, "token")
        self.assertRangesEqual(ranges, 5, "wildlife", 33, 41, "token")
        self.assertRangesEqual(ranges, 6, "foundation", 42, 52, "token")
        self.assertRangesEqual(ranges, 7, "goodly", 59, 65, "token")
        self.assertRangesEqual(ranges, 8, "goodly gracious sakes", 59, 80, "token")
        self.assertRangesEqual(ranges, 9, "gracious", 66, 74, "token")
        self.assertRangesEqual(ranges, 10, "sakes", 75, 80, "token")

    def test_joiners(self):
        ranges = wildgram("of",stopwords=["of"], returnNoise=False,includeParent=False)
        self.assertListEqual(ranges, [])
        ranges = wildgram("of milady", stopwords=["of"], returnNoise=False,includeParent=False)
        self.assertRangesEqual(ranges, 0, "milady", 3, 9,"token")

        ranges = wildgram("milady of perpetual redemption", stopwords=["of"],include1gram=False, returnNoise=False,includeParent=False)
        self.assertRangesEqual(ranges, 1, "milady of perpetual redemption", 0, 30,"token")
        self.assertRangesEqual(ranges, 0, "milady", 0, 6,"token")
        self.assertRangesEqual(ranges, 2, "perpetual redemption", 10, 30,"token")
        ranges = wildgram("milady of the perpetual redemption", stopwords=["of", "the"],include1gram=False, returnNoise=False,includeParent=False)
        self.assertRangesEqual(ranges, 0, "milady", 0, 6,"token")
        self.assertRangesEqual(ranges, 1, "milady of the perpetual redemption", 0, 34,"token")
        self.assertRangesEqual(ranges, 2, "perpetual redemption", 14, 34,"token")

        ranges = wildgram("milady of the perpetual redemption, she's great and of sound mind", stopwords=["of", "the"],topicwords=[],include1gram=False, returnNoise=False,includeParent=False)
        self.assertRangesEqual(ranges, 0, "milady", 0, 6,"token")
        self.assertRangesEqual(ranges, 1, "milady of the perpetual redemption", 0, 34,"token")
        self.assertRangesEqual(ranges, 2, "perpetual redemption", 14, 34,"token")
        self.assertRangesEqual(ranges, 3, "she's great and", 36, 51,"token")
        self.assertRangesEqual(ranges, 4, "she's great and of sound mind", 36, 65,"token")
        self.assertRangesEqual(ranges, 5, "sound mind", 55, 65,"token")

    def test_noise(self):
        ranges = wildgram("test.", returnNoise=False)
        self.assertRangesEqual(ranges,0, "test", 0, 4, "token")
        self.assertEqual(len(ranges), 1)

        ranges= wildgram("of",stopwords=["of"], returnNoise=True)
        self.assertRangesEqual(ranges, 0, "of", 0, 2,"noise")

        ranges = wildgram("of milady", stopwords=["of"], returnNoise=True)
        self.assertRangesEqual(ranges, 0, "of ", 0, 3,"noise")
        self.assertRangesEqual(ranges, 1, "milady", 3, 9,"token")

        ranges= wildgram("milady of", stopwords=["of"], returnNoise=True)
        self.assertRangesEqual(ranges, 0, "milady", 0, 6,"token")
        self.assertRangesEqual(ranges, 1, " of", 6, 9,"noise")

        ranges = wildgram("milady of perpetual redemption", stopwords=["of"],include1gram=False, returnNoise=True)
        self.assertRangesEqual(ranges, 2, " of ", 6, 10, "noise")

        ranges = wildgram("milady of the perpetual redemption", stopwords=["of", "the"],include1gram=False, returnNoise=True)
        self.assertRangesEqual(ranges, 2, " of the ", 6, 14, "noise")

        txt = "milady of the perpetual redemption, she's great and of sound mind"
        ranges = wildgram(txt, stopwords=["of", "the"],topicwords=[],include1gram=False, returnNoise=True)
        self.assertRangesEqual(ranges, 2, " of the ", 6, 14, "noise")
        self.assertRangesEqual(ranges, 4, ", ", 34, 36, "noise")
        self.assertRangesEqual(ranges, 7, " of ", 51, 55, "noise")

        ranges= wildgram("milady of the perpetual redemption, she's great and of sound mind. shortness of breath", stopwords=["of", "the"], topicwords=[], include1gram=True,returnNoise=True)
        self.assertRangesEqual(ranges, 2, " of the ", 6, 14, "noise")
        self.assertRangesEqual(ranges, 5, " ", 23, 24, "noise")
        self.assertRangesEqual(ranges, 7, ", ", 34, 36, "noise")
        self.assertRangesEqual(ranges, 11, "'", 39, 40, "noise")
        self.assertRangesEqual(ranges, 13, " ", 41, 42, "noise")
        self.assertRangesEqual(ranges, 15, " ", 47, 48, "noise")
        self.assertRangesEqual(ranges, 17, " of ", 51, 55, "noise")
        self.assertRangesEqual(ranges, 20, " ", 60, 61, "noise")
        self.assertRangesEqual(ranges, 22, ". ", 65, 67, "noise")
        self.assertRangesEqual(ranges, 25, " of ", 76, 80, "noise")

        ranges = wildgram("12312312 | 123213| alaska",returnNoise=True)
        self.assertRangesEqual(ranges, 1, " | ", 8, 11, "noise")
        self.assertRangesEqual(ranges, 3, "| ", 17, 19, "noise")
        ranges = wildgram("testing-123", returnNoise=True)
        self.assertEqual(len(ranges), 1)
        ranges = wildgram("testing-abc", returnNoise=True)
        self.assertEqual(len(ranges), 1)
        ranges = wildgram("butalbital-apap-caffeine (bac oral tablet 50-325-40 mg)", returnNoise = True)
        self.assertRangesEqual(ranges, 0, "butalbital-apap-caffeine", 0, 24, "token")


    def test_token_types(self):
        ranges = wildgram("1231232 1231231", topicwords=[{"token":"\d+", "tokenType": "numeric"}])
        self.assertRangesEqual(ranges, 0, "1231232", 0, 7,"numeric")
        ranges = wildgram("123.1232 1231231")
        self.assertRangesEqual(ranges, 0, "123.1232", 0, 8, "numeric")
        self.assertRangesEqual(ranges, 1, " ", 8,9, "noise")
        self.assertRangesEqual(ranges, 2, "1231231", 9, 16, "numeric")

    def test_wildrules(self):
        rules = [{"unit": "TEST", "value": "boo", "spans": ["testing", "test"], "spanType": "token"}]
        test = WildRules(rules)
        ret = test.apply("")
        self.assertEqual(ret, [])
        ret = test.apply("testing 123")
        self.assertEqual([{"unit": "TEST", "value": "boo", 'token': 'testing', 'startIndex': 0, 'endIndex': 7}, {'unit': 'unknown', 'value': 'unknown', 'token': '123', 'startIndex': 8, 'endIndex': 11}], ret)
        test= WildRules([{"unit": "TEST", "value": "unknown", "spans": ["testing", "test"], "spanType": "token"}, {"unit": "Dosage", 'value': {"spanType": "token", "asType": "float"}, "spans": ["numeric"], "spanType": "tokenType"}])
        ret = test.apply("testing test 123")
        self.assertEqual([{"unit": "TEST", "value": "unknown", 'token': 'testing test', 'startIndex': 0, 'endIndex': 12}, {'unit': 'Dosage', 'value': 123.0, 'token': "123", 'startIndex': 13, 'endIndex': 16}], ret)
        test= WildRules([{"unit": "TEST", "value": "unknown", "spans": ["testing", "test"], "spanType": "token", "nearby": [{"spanType": "token", "spans": ["1234"]}]}, {"unit": "Dosage", "value":{"spanType": "token", "asType": "float"}, "spans": ["numeric"], "spanType": "tokenType"}])
        ret = test.apply("testing test 123")
        self.assertEqual([{'unit': 'unknown', "value": "unknown", 'token': 'testing test', 'startIndex': 0, 'endIndex': 12}, {'unit': 'Dosage', "value": 123.0, 'token': '123', 'startIndex': 13, 'endIndex': 16}], ret)
        ret = test.apply("testing test 1234")
        self.assertEqual([{'unit': 'TEST','value': "unknown", 'token': 'testing test', 'startIndex': 0, 'endIndex': 12}, {'unit': 'Dosage', "value": 1234.0, 'token': '1234', 'startIndex': 13, 'endIndex': 17}], ret)

        test= WildRules([{"unit": "TEST", "value": "unknown", "spans": ["testing", "test"], "spanType": "token", "nearby": [{"spanType": "token", "spans": ["1234"], "startOffset": -10, "endOffset": 0}]}, {"unit": "Dosage", "value": "unknown",  "spans": ["numeric"], "spanType": "tokenType"}])
        ret = test.apply("testing test 1234")
        self.assertEqual([{'unit': 'unknown', "value": "unknown", 'token': 'testing test', 'startIndex': 0, 'endIndex': 12}, {'unit': 'Dosage', "value": "unknown", 'token': '1234', 'startIndex': 13, 'endIndex': 17}], ret)
        ret = test.apply("1234 testing test")
        self.assertEqual([{'unit': 'Dosage', "value": "unknown", 'token': '1234', 'startIndex': 0, 'endIndex': 4}, {'unit': 'TEST',  "value": "unknown", 'token': 'testing test', 'startIndex': 5, 'endIndex': 17}], ret)
        test = WildRules([{"unit": "distended", "value": "unknown", "spans" :["distended", "-distended"], "spanType": "token"},{"unit": "pain", "value": "pain",  "spans": ["pain"], "spanType": "token"},{"unit": "unknown", "value": "unknown", "spans": ["pain"], "spanType": "token", "nearby":[{"spanType": "token", "spans": ["medication", "regimen", "medications","control"], "startOffset": 0, \
"endOffset": 3},{"spanType" :"token", "spans": ["while in"]}]}])
        span = "10 lbs) for at least 6 weeks. No driving while in pain or on narcotic pain medications. Nothing in the v"
        print(test.apply(span))
        self.assertEqual(test.apply(span), [{'unit': 'unknown', 'value': 'unknown', 'token': '10 lbs', 'startIndex': 0, 'endIndex': 6}, {'unit': 'unknown', 'value': 'unknown', 'token': 'least 6 weeks', 'startIndex': 15, 'endIndex': 28}, {'unit': 'unknown', 'value': 'unknown','token': 'No driving', 'startIndex': 30, 'endIndex': 40}, {'unit': 'unknown', 'value': 'unknown', 'token': 'pain', 'startIndex': 50, 'endIndex': 54}, {'unit': 'unknown', 'value': 'unknown', 'token': 'narcotic pain medications', 'startIndex': 61, 'endIndex': 86}, {'unit': 'unknown', 'value': 'unknown', 'token': 'Nothing', 'startIndex': 88, 'endIndex': 95}, {'unit': 'unknown', 'value': 'unknown','token': 'v', 'startIndex': 103, 'endIndex': 104}])
        self.assertEqual(test.apply("ipheral edema\nGI/Abdominal: soft, non-tender, non-distended, NABS\nSkin: No jaundice; diffuse scattered seborr"),[{'unit': 'unknown', 'value': 'unknown', 'token': 'ipheral edema\nGI/Abdominal', 'startIndex': 0, 'endIndex': 26}, {'unit': 'unknown', 'value': 'unknown','token': 'soft', 'startIndex': 28, 'endIndex': 32}, {'unit': 'unknown', 'value': 'unknown', 'token': 'non-tender', 'startIndex': 34, 'endIndex': 44}, {'unit': 'unknown', 'value': 'unknown', 'token': 'non-', 'startIndex': 46, 'endIndex': 50}, {'unit': 'distended', 'value': 'unknown', 'token': 'distended', 'startIndex': 50, 'endIndex': 59}, {'unit': 'unknown', 'value': 'unknown', 'token': 'NABS\nSkin', 'startIndex': 61, 'endIndex': 70}, {'unit': 'unknown', 'value': 'unknown', 'token': 'No jaundice', 'startIndex': 72, 'endIndex': 83}, {'unit': 'unknown', 'value': 'unknown','token': 'diffuse scattered seborr', 'startIndex': 85, 'endIndex': 109}])
        test = WildRules([{"unit":"WHITE", "value": "unknown", "spans": [""], "spanType": "token"}])
        self.assertEqual(test.apply("  "), [{'endIndex': 2, 'startIndex': 0, 'token': '', 'unit': 'WHITE', 'value': 'unknown',}])
    def test_wildform(self):
        test = WildForm()
        test.add_form({"unit": "test", "value": "testing", "children": [{"unit": "test", "value": "", "children": []}]})
        self.assertEqual(test.forms, [{'unit': 'test', 'value': 'testing', 'children': [{'unit': 'test', 'value': '', 'children': []}]}])
        self.assertEqual(test.leaves, [{'unit': 'test', 'value': '', 'children': []}])
        self.assertEqual(test.leafToLeafKey, {0: ['0-0']})
        rules = WildRules([{"unit": "test", "value": "boo", "spans": ["testing", "test"], "spanType": "token"}])
        tokens = rules.apply("testing 123, can anyone hear me?")
        self.assertEqual(test.apply(tokens), [{'unit': 'test', 'value': 'testing', 'children': [{'unit': 'test', 'value': 'boo', 'children': [], 'startIndex': 0, 'endIndex': 7, 'token': 'testing'}]}])
        ret = test.apply(rules.apply("testing 123, can anyone hear me? testing 1234"))
        self.assertEqual(ret, [{'unit': 'test', 'value': 'testing', 'children': [{'unit': 'test', 'value': 'boo', 'children': [], 'startIndex': 0, 'endIndex': 7, 'token': 'testing'}]}, {'unit': 'test', 'value': 'testing', 'children': [{'unit': 'test', 'value': 'boo', 'children': [], 'startIndex': 33, 'endIndex': 40, 'token': 'testing'}]}])
        test.add_form({"unit": "test", "value": "testing", "children": [{"unit": "test", "value": "", "children": []}, {"unit": "Dosage", "value": "", "children": []}]})
        rules = WildRules([{"unit": "test", "value": "unknown", "spans": ["testing", "test"], "spanType": "token"}, {"unit": "Dosage", 'value': {"spanType": "token", "asType": "float"}, "spans": ["numeric"], "spanType": "tokenType"}])
        ret = test.apply(rules.apply("testing, can anyone hear me? testing 1234"))
        self.assertEqual(ret, [{'unit': 'test', 'value': 'testing', 'children': [{'unit': 'test', 'value': 'unknown', 'children': [], 'startIndex': 0, 'endIndex': 7, 'token': 'testing'}]}, {'unit': 'test', 'value': 'testing', 'children': [{'unit': 'test', 'value': 'unknown', 'children': [], 'startIndex': 0, 'endIndex': 7, 'token': 'testing'}, {'unit': 'Dosage', 'value': '', 'children': []}]}, {'unit': 'test', 'value': 'testing', 'children': [{'unit': 'test', 'value': 'unknown', 'children': [], 'startIndex': 29, 'endIndex': 36, 'token': 'testing'}]}, {'unit': 'test', 'value': 'testing', 'children': [{'unit': 'test', 'value': 'unknown', 'children': [], 'startIndex': 29, 'endIndex': 36, 'token': 'testing'}, {'unit': 'Dosage', 'value': 1234.0, 'children': [], 'startIndex': 37, 'endIndex': 41, 'token': '1234'}]}])
        test = WildForm()
        test.add_form({"unit": "Trigger", "value": "", "children": [{"unit": "Duration", "value": "", "children": [{"unit":"EHR", "value": "META-FORM", "children": [{"unit": "EHR", "value": "INTRA-FORM-SHARABLE", "children":[]}]}]}]})
        rules = WildRules([{"unit": "Trigger", "value": {"spanType":"token"}, "spanType": "token", "spans": ["pain", "fatigue", "nausea", "vomiting", "diarrhea"]}, {"unit": "Duration", "value": {"spanType": "token"}, "spanType": "token", "spans": ["chronic", "acute"], "nearby": [{"spanType": "unit", "spans": ["Trigger"]}]}])
        spans = "in a while.  There is no numbness or tingling but pain with walking any distance.  (R53.82) Chronic fatigue"
        self.assertEqual(test.apply(rules.apply(spans.lower())),[{'unit': 'Trigger', 'value': 'pain', 'children': [{'unit': 'Duration', 'value': '', 'children': [{'unit': 'EHR', 'value': 'META-FORM', 'children': [{'unit': 'EHR', 'value': 'INTRA-FORM-SHARABLE', 'children': []}]}]}], 'startIndex': 50, 'endIndex': 54, 'token': 'pain'}, {'unit': 'Trigger', 'value': 'fatigue', 'children': [{'unit': 'Duration', 'value': 'chronic', 'children': [{'unit': 'EHR', 'value': 'META-FORM', 'children': [{'unit': 'EHR', 'value': 'INTRA-FORM-SHARABLE', 'children': []}]}], 'startIndex': 92, 'endIndex': 99, 'token': 'chronic'}], 'startIndex': 100, 'endIndex': 107, 'token': 'fatigue'}])
        spans = "acute onset abdominal pain, nausea, vomiting, and diarrhea."
        res = test.apply(rules.apply(spans.lower()))
        self.assertEqual(res,[{'unit': 'Trigger', 'value': 'pain', 'children': [{'unit': 'Duration', 'value': 'acute', 'children': [{'unit': 'EHR', 'value': 'META-FORM', 'children': [{'unit': 'EHR', 'value': 'INTRA-FORM-SHARABLE', 'children': []}]}], 'startIndex': 0, 'endIndex': 5, 'token': 'acute'}], 'startIndex': 22, 'endIndex': 26, 'token': 'pain'}, {'unit': 'Trigger', 'value': 'nausea', 'children': [{'unit': 'Duration', 'value': 'acute', 'children': [{'unit': 'EHR', 'value': 'META-FORM', 'children': [{'unit': 'EHR', 'value': 'INTRA-FORM-SHARABLE', 'children': []}]}], 'startIndex': 0, 'endIndex': 5, 'token': 'acute'}], 'startIndex': 28, 'endIndex': 34, 'token': 'nausea'}, {'unit': 'Trigger', 'value': 'vomiting', 'children': [{'unit': 'Duration', 'value': 'acute', 'children': [{'unit': 'EHR', 'value': 'META-FORM', 'children': [{'unit': 'EHR', 'value': 'INTRA-FORM-SHARABLE', 'children': []}]}], 'startIndex': 0, 'endIndex': 5, 'token': 'acute'}], 'startIndex': 36, 'endIndex': 44, 'token': 'vomiting'}, {'unit': 'Trigger', 'value': 'diarrhea', 'children': [{'unit': 'Duration', 'value': 'acute', 'children': [{'unit': 'EHR', 'value': 'META-FORM', 'children': [{'unit': 'EHR', 'value': 'INTRA-FORM-SHARABLE', 'children': []}]}], 'startIndex': 0, 'endIndex': 5, 'token': 'acute'}], 'startIndex': 50, 'endIndex': 58, 'token': 'diarrhea'}])
        spans = "acute onset abdominal pain, blah - blah - blah - blah - nausea."
        res = test.apply(rules.apply(spans.lower()))
        self.assertEqual(res, [{'unit': 'Trigger', 'value': 'pain', 'children': [{'unit': 'Duration', 'value': 'acute', 'children': [{'unit': 'EHR', 'value': 'META-FORM', 'children': [{'unit': 'EHR', 'value': 'INTRA-FORM-SHARABLE', 'children': []}]}], 'startIndex': 0, 'endIndex': 5, 'token': 'acute'}], 'startIndex': 22, 'endIndex': 26, 'token': 'pain'}, {'unit': 'Trigger', 'value': 'nausea', 'children': [{'unit': 'Duration', 'value': '', 'children': [{'unit': 'EHR', 'value': 'META-FORM', 'children': [{'unit': 'EHR', 'value': 'INTRA-FORM-SHARABLE', 'children': []}]}]}], 'startIndex': 56, 'endIndex': 62, 'token': 'nausea'}])


class TestMagicRules(unittest.TestCase):
    def test_wildrules(self):
        system = MagicRules()
        system.add_trigger("testing",{"unit": "Boo", "value":"TEST", "children": []})
        self.assertEqual(system.predict("testing? 123"), [{'unit': 'Boo', 'value': 'TEST', 'startIndex': 0, 'endIndex': 7, 'snippet': 'testing',"children": []}])
        self.assertEqual(system.predict("test? 123"), [])
        system.train_one_document("test? 123", [{'unit': 'Boo', 'value': 'TEST', 'startIndex': 0, 'endIndex': 4, 'snippet': 'test'}])
        self.assertEqual(system.predict("test? 123"),[{'unit': 'Boo', 'value': 'TEST', 'startIndex': 0, 'endIndex': 4, 'snippet': 'test', "children": []}])

        system = MagicRules()
        system.add_trigger("discharge",{"unit": "Symptom", "value": "Vaginal Discharge", "children": []}, ["vaginal"])
        system.add_trigger("discharge", {"unit": "Symptom", "value": "Vaginal Discharge", "children": [{"unit": "Color", "value": "Yellow"}]}, ["yellow", "vaginal"])
        self.assertEqual(system.predict("discharge instructions: please be careful crossing the street."), [])#[{'unit': 'Symptom', 'value': 'Vaginal Discharge', 'startIndex': 0, 'endIndex': 9, 'snippet': 'discharge',"children": [{"unit": "Color", "value": "Yellow"}]}])
        system.train_one_document("discharge instructions: please be careful crossing the street.", [])
        system.train_one_document("vaginal discharge is yellow.", [{"unit": "Symptom", "value": "Vaginal Discharge", "startIndex": 8, "endIndex": 17, "snippet": "discharge", "children": [{"unit": "Color", "value": "Yellow"}]}])
        system.train_one_document("bloody vaginal discharge.", [{"unit": "Symptom", "value": "Vaginal Discharge", "startIndex": 15, "endIndex": 24,"snippet": "discharge","children": []}])
        system.train_one_document("discharge (vaginal) is concerning", [{"unit": "Symptom", "value": "Vaginal Discharge", "startIndex": 0, "endIndex": 9,"snippet": "discharge","children": []}])
        self.assertEqual(system.predict("discharge instructions are complete."), [])
        self.assertEqual(system.predict("please follow the discharge procedure."), [])
        self.assertEqual(system.predict("your bloody vaginal discharge is a problem"),[{'unit': 'Symptom', 'value': 'Vaginal Discharge', 'startIndex': 20, 'endIndex': 29, 'snippet': 'discharge',"children": []}])
        self.assertEqual(system.predict("discharge discharge instructions"), [])
        self.assertEqual(system.predict("her vaginal discharge is concerning, however the vaginal canal seems healthy."),[{'unit': 'Symptom', 'value': 'Vaginal Discharge', 'children': [], 'startIndex': 12, 'endIndex': 21, 'snippet': 'discharge'}])
        self.assertEqual(system.predict("her vaginal discharge is concerning, however the vaginal canal seems healthy.", debug=True),([{'unit': 'Symptom', 'value': 'Vaginal Discharge', 'children': [], 'startIndex': 12, 'endIndex': 21, 'snippet': 'discharge'}], [{'unit': 'Symptom', 'value': 'Vaginal Discharge', 'children': [], 'startIndex': 12, 'endIndex': 21, 'snippet': 'discharge'}], {'Symptom|||Vaginal Discharge': [2]}))
        system.train_one_document("vaginal discharge is saffron colored.", [{"unit": "Symptom", "value": "Vaginal Discharge", "startIndex": 8, "endIndex": 17, "snippet": "discharge", "children": [{"unit": "Color", "value": "Yellow"}]}])
        self.assertEqual(system.Triggers, {'discharge': {'Symptom|||Vaginal Discharge': {'discharge': 5, 'vaginal': 7.0, 'bloody': 1, 'bloody vaginal discharge': 1, 'concerning': 1}, 'Symptom|||Vaginal Discharge[Color|||Yellow]': {'discharge': 5, 'yellow': 3.5, 'vaginal': 4.5, 'vaginal discharge': 2, 'saffron': 1, 'saffron colored': 1, 'colored': 1}}})

    def test_umls(self):
        apikey = input("please give your api key for UMLS...")
        system = MagicRules()
        # pelvic pain
        system.add_umls_cui("C0478659", apikey)
        # abdominal pain
        system.add_umls_cui("C0000737", apikey)
        self.assertEqual(system.predict("Patient has both pelvic and abdominal pain."),[{'unit': 'UMLS', 'value': 'C0478659', 'startIndex': 17, 'endIndex': 42, 'snippet': 'pelvic and abdominal pain', 'children': []}, {'unit': 'UMLS', 'value': 'C0000737', 'startIndex': 28, 'endIndex': 42, 'snippet': 'abdominal pain','children': []}])
        system.train_one_document("finding of gastrointestinal dysfunction", [])
        self.assertEqual(system.predict("finding of gastrointestinal dysfunction"), [])
        system.train_one_document("finding of abdominal pain", [{'unit': 'UMLS', 'value': 'C0000737', 'startIndex': 0, 'endIndex': 7, 'snippet': 'finding', "children": []}, {'unit': 'UMLS', 'value': 'C0000737', 'startIndex': 11, 'endIndex': 25, 'snippet': 'abdominal pain',"children": []}])
        self.assertEqual(system.predict("finding of abdominal pain"),[{'unit': 'UMLS', 'value': 'C0000737', 'startIndex': 0, 'endIndex': 7, 'snippet': 'finding',"children": []}, {'unit': 'UMLS', 'value': 'C0000737', 'startIndex': 11, 'endIndex': 25, 'snippet': 'abdominal pain',"children": []}])

    def test_code_coverter(self):
        basic = {"unit": "blah", "value": "boo", "children": []}
        self.assertEqual(convertCodeDictToString(basic), "blah|||boo")

        basic = {"unit": "blah", "value": "boo", "children": [{"unit": "boop", "value": "beep", "children": []}]}
        self.assertEqual(convertCodeDictToString(basic), "blah|||boo[boop|||beep]")

        basic = {"unit": "blah", "value": "boo", "children": [{"unit": "coop", "value": "ceep", "children": []},{"unit": "boop", "value": "beep", "children": []}]}
        sortedbasic = {"unit": "blah", "value": "boo", "children": [{"unit": "boop", "value": "beep", "children": []},{"unit": "coop", "value": "ceep", "children": []}]}

if __name__ == '__main__':
    unittest.main()
